/**
 * Johnny Tsheke @ UQAM -- INF1256
 */
package lab07;

import java.util.*;
public class CaisseEpicier {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CaisseEpicier ce = new CaisseEpicier();
		double txTPS = 0.05;
		double txTVQ = 0.0975;
		double prixArticle =0.0;
		prixArticle = ce.scannePrix();
		ce.afficheFacture(txTPS, txTVQ, prixArticle);
	}
	
    double scannePrix(){
    	Scanner clavier = new Scanner(System.in);
    	double prixSaisi = 0.0;
    	System.out.println("Veuillez saisir le prix du prochain article svp");
    	while(!clavier.hasNextDouble()){
    		System.out.println("Réponse invalide! Veuillez saisir un montant valide svp!");
    		clavier.next();
    	}
    	prixSaisi = clavier.nextDouble(); //ici le prix est valide
    	clavier.close();
    	return(prixSaisi);
    	
    }
    
    void afficheFacture(double tauxTps, double tauxTvq, double prix){
    	double montantTPS = tauxTps * prix;
    	double montantTVQ = tauxTvq * prix;
    	double montantTotal = prix + montantTPS + montantTVQ;
    	System.out.format("-- Voici votre facture --%n");
    	System.out.format("Le montant avant taxe est de: %.2f%n",prix);
    	System.out.format("Le montant TPS est de: %.2f%n",montantTPS);
    	System.out.format("Le montant TVQ est de: %.2f%n",montantTVQ);
    	System.out.format("Le montant total est de: %.2f%n",montantTotal);
    	
    }
}
